﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace proiect_final
{
    class Control
    {

        List<Abonati> A = new List<Abonati>();
        private const string STORED_PROCEDURE_LOAD = "LOADALL";
        private const string STORED_PROCEDURE_SEARCH = "Cautare";
        private const string STORED_PROCEDURE_DELETE = "Stergere";
        private const string STORED_PROCEDURE_SAVE_PROC = "SAVE_PROC";
        SqlConnection con1 = new SqlConnection();

        public Control(DataGridView GridView1)
        {
            OpenConnection();
            //BindingSource to sync DataTable and DataGridView
           // BindingSource bSource = new BindingSource();
            //set the BindingSource DataSource
          //  bSource.DataSource = dt;
            //set the DataGridView DataSource
          //  GridView1.DataSource = bSource;
        }

        public void OpenConnection()
        {
            con1.ConnectionString = "Data Source = XYZ-PC; Initial Catalog = Abonati; Integrated Security = true;";

            try
            {
                con1.Open();
                //if (con1.State == ConnectionState.Open)
                   // MessageBox.Show("Sunteti conectat!");
            }
            catch
            {
                MessageBox.Show("Nu se poate realiza conexiunea la baza de date!!!");
            }
            finally
            {
                if (con1 != null)
                {
                    con1.Close();
                }
            }
        }

         public void Search(DataGridView GridView1, TextBox SearchName, TextBox SearchSurname, TextBox SearchAdr, TextBox SearchTel)
         {
            con1.Open();
            string[] param1 = new string[4];
            string[] param2 = new string[4];
            int nr = 0;
            int[] v = new int[4];

            GetData(ref param1, ref param2, ref nr, ref v, 4, SearchName, SearchSurname, SearchAdr, SearchTel);

            //Abonati A = new Abonati(param2[0], param2[1], param2[2], param2[3]);
            //A.Search(GridView1);

            ExecuteProcedure(GridView1, STORED_PROCEDURE_SEARCH, param1, param2, nr);
            con1.Close();
         }

        public void Insert(TextBox AddName, TextBox AddSurname, TextBox AddAddr, TextBox AddTel, DataGridView GD)
        {
            con1.Open();
            int ok = 0;
            SqlCommand command = new SqlCommand();
            command.Connection = con1;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = STORED_PROCEDURE_SAVE_PROC;
            command.Parameters.Add(new SqlParameter("@var", ok));
            command.Parameters.Add(new SqlParameter("@param1", AddName.Text));
            command.Parameters.Add(new SqlParameter("@param2", AddSurname.Text));
            command.Parameters.Add(new SqlParameter("@param3", AddAddr.Text));
            command.Parameters.Add(new SqlParameter("@param4", AddTel.Text));

            command.ExecuteNonQuery();
            con1.Close();
            Load(GD);
        }

        public void Delete(DataGridView GridView1, TextBox DeleteName, TextBox DeleteSurname, TextBox DeleteTel, DataGridView GD)
        {
            con1.Open();
            string[] param1 = new string[3];
            string[] param2 = new string[3];
            int nr = 0;
            int[] v = new int[4];

            GetData(ref param1, ref param2, ref nr, ref v, 3, DeleteName, DeleteSurname, DeleteTel);

            ExecuteProcedure_Simple(STORED_PROCEDURE_DELETE, param1, param2, nr);
            con1.Close();
            Load(GD);
        }

        public void Update(TextBox UpdateName, TextBox UpdateSurname, TextBox UpdateAdr, TextBox UpdateNrTel, DataGridView GD)
        {
            con1.Open();

            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con1;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = STORED_PROCEDURE_SAVE_PROC;
            int var = 0;

            if (UpdateAdr.TextLength != 0 && UpdateNrTel.TextLength != 0)
                var = 3;
            else
                if (UpdateAdr.TextLength != 0 && UpdateNrTel.TextLength == 0)
                    var = 1;
                else
                    if (UpdateAdr.TextLength == 0 && UpdateNrTel.TextLength != 0)
                        var = 2;

            cmd.Parameters.AddWithValue("@var", var);
            cmd.Parameters.AddWithValue("@param1", UpdateName.Text);
            cmd.Parameters.AddWithValue("@param2", UpdateSurname.Text);

            if (var == 3)
            {
                cmd.Parameters.AddWithValue("@param3", UpdateAdr.Text);
                cmd.Parameters.AddWithValue("@param4", UpdateNrTel.Text);
            }
            else
                if (var == 1)
                    cmd.Parameters.AddWithValue("@param3", UpdateAdr.Text);
                else
                    if (var == 2)
                        cmd.Parameters.AddWithValue("@param3", UpdateNrTel.Text);


            cmd.ExecuteNonQuery();
            con1.Close();
            Load(GD);
        }

        public void Load(DataGridView gridView1)
        {
            con1.Open();
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("Nume", typeof(string));
            dt.Columns.Add("Prenume", typeof(string));
            dt.Columns.Add("Adresa", typeof(string));
            dt.Columns.Add("NrTel", typeof(string));

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = STORED_PROCEDURE_LOAD;
            cmd.Connection = con1;

            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                Abonati item = new Abonati((int)reader[0], reader[1].ToString(), reader[2].ToString(),
                                           reader[3].ToString(), reader[4].ToString());
                A.Add(item);
            }

            reader.Close();
            DataRow workRow;
            for (int row = 0; row < A.Count(); row++)
            {
                string[] currentColumn = new string[A.Count()];

                workRow = dt.NewRow();
                workRow[0] = A[row].id;
                workRow[1] = A[row].Nume;
                workRow[2] = A[row].Prenume;
                workRow[3] = A[row].Adresa;
                workRow[4] = A[row].NrTel;

                dt.Rows.Add(workRow);
            }
            gridView1.DataSource = dt;
            A.Clear();
            con1.Close();
        }

        public void ExecuteProcedure(DataGridView GridView1, string procedure, string[] param1, string[] param2, int nr)
        {
            SqlCommand command;
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();
            command = new SqlCommand(procedure);
            command.Connection = con1;
            command.CommandType = CommandType.StoredProcedure;
            int l = 1;

            for (int i = 0; i < nr; i++)
            {
                string x = "@" + "param" + l.ToString();
                command.Parameters.Add(new SqlParameter(x, param1[i]));

                //MessageBox.Show(x);

                x = "@" + "val" + l.ToString();
                command.Parameters.Add(new SqlParameter(x, param2[i]));
                l++;
                //MessageBox.Show(x);
            }

            da.SelectCommand = command;

            //da.Fill(dt);
            da.Fill(dt);
            GridView1.DataSource = dt;
        }

        public void ExecuteProcedure_Simple(string procedure, string[] param1, string[] param2, int nr)
        {
            SqlCommand command;

            command = new SqlCommand(procedure);
            command.Connection = con1;
            command.CommandType = CommandType.StoredProcedure;
            int l = 1;

            for (int i = 0; i < nr; i++)
            {
                string x = "@" + "param" + l.ToString();
                command.Parameters.Add(new SqlParameter(x, param1[i]));

                //MessageBox.Show(x);

                x = "@" + "val" + l.ToString();
                command.Parameters.Add(new SqlParameter(x, param2[i]));
                l++;
                //MessageBox.Show(x);
            }

            command.ExecuteNonQuery();
        }

        //extBox SearchName, TextBox SearchSurname, TextBox SearchAdr, TextBox SearchTel)

        void GetData(ref string[] param1, ref string[] param2, ref int nr, ref int[] v, int NR, params TextBox[] TB)
        {

            for (int i = 0; i < NR; i++)
            {
                param1[i] = "";
                param2[i] = "";
            }

            v[0] = TB[0].Text.Length;
            if (v[0] != 0)
            {
                param1[nr] = "Nume";
                param2[nr] = TB[0].Text;
                nr++;
            }

            v[1] = TB[1].Text.Length;
            if (v[1] != 0)
            {
                param1[nr] = "Prenume";
                param2[nr] = TB[1].Text;
                nr++;
            }

            v[2] = TB[2].Text.Length;
            if (v[2] != 0)
            {
                param1[nr] = "Adresa";
                param2[nr] = TB[2].Text;
                nr++;
            }

            if (TB.Count() == 4)
            {
                v[3] = TB[3].Text.Length;
                if (v[3] != 0)
                {
                    param1[nr] = "NrTel";
                    param2[nr] = TB[3].Text;
                    nr++;
                }
            }

        }
    }
}
