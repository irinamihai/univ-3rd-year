﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace proiect_final
{
    public partial class Form2 : Form
    {

        SqlConnection con;
        Control C;
        int LEVEL;

        public Form2(string UserLevel)
        {
            InitializeComponent();
           // GridView1.ColumnCount = 5;
            LEVEL = Convert.ToInt32(UserLevel);
            OpenConnection();            

            C = new Control(GridView1);
           // Load();
        }

        public void OpenConnection()
        {
            con = new SqlConnection("Data Source = XYZ-PC; Initial Catalog = Abonati; Integrated Security = true;");
            try
            {
                con.Open();
             /*   if (con.State == ConnectionState.Open)
                    MessageBox.Show("Sunteti conectat!");*/
            }
            catch
            {
                MessageBox.Show("Nu se poate realiza conexiunea la baza de date!!!");
            }
            /*finally
            {
                if (con != null)
                    con.Close();
            }*/
        }

      
        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (LEVEL < 3)
            {
                MessageBox.Show("Nu aveti permisiune!!!");
                return;
            }

            if (UpdateName.TextLength == 0 && UpdateSurname.TextLength == 0)
                MessageBox.Show("Trebuie sa completati numele si prenumele");
           else
                if (Check_Nr_Tel(UpdateNrTel.Text) == false)
                {
                    MessageBox.Show("Nu ai introdus corect nr de tel");
                    DeleteTel.Clear();
                    return;
                }
                else
                    if (UpdateNrTel.TextLength != 0 && UpdateNrTel.TextLength != 10)
                    {
                        MessageBox.Show("Nr de tel nu are nr corespunzator de cifre");
                        UpdateNrTel.Clear();
                        return;
                    }
                    else
                    {
                        C.Update(UpdateName, UpdateSurname, UpdateAdr, UpdateNrTel, GridView1);
                    }

            UpdateName.Clear();
            UpdateSurname.Clear();
            UpdateAdr.Clear();
            UpdateNrTel.Clear();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {

            if (Check_Nr_Tel(SearchTel.Text) == false)
            {
                MessageBox.Show("Nu ai introdus corect nr de tel");
                SearchTel.Clear();
                return;
            }
                else
                    if (SearchTel.TextLength != 0 && SearchTel.TextLength != 10)
                    {
                        MessageBox.Show("Nr de tel nu are nr corespunzator de cifre");
                        SearchTel.Clear();
                        return;
                    }
           
            C.Search(GridView1, SearchName, SearchSurname, SearchAdr, SearchTel);
        }

        private bool Check_Nr_Tel(string s)
        { 
            foreach(char c in s)
                if(c < '0' || c > '9')
                    return false;

            return true;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (LEVEL < 2)
            {
                MessageBox.Show("Nu aveti permisiune!!!");
                return;
            }
         
            if (AddName.Text.Length == 0 || AddSurname.Text.Length == 0 ||
               AddTel.Text.Length == 0 || AddAddr.Text.Length == 0)
            {
                MessageBox.Show("Nu ai introdus toate campurile necesare!!!");
                return;
            }

            if (Check_Nr_Tel(AddTel.Text) == false)
            {
                MessageBox.Show("Nu ai introdus corect nr de tel");
                AddTel.Clear();
                return;
            }

            if (AddTel.TextLength != 0 && AddTel.TextLength != 10)
            {
                MessageBox.Show("Nr de tel nu are nr corespunzator de cifre");
                AddTel.Clear();
                return;
            }
            C.Insert(AddName, AddSurname, AddAddr ,AddTel, GridView1);
            AddName.Clear();
            AddSurname.Clear();
            AddAddr.Clear();
            AddTel.Clear();
  
        }

        private void GridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show("S-a schimbat cv!!");
            //Aici sa se faca update!!!
        }

        private void load_Click(object sender, EventArgs e)
        {
            C.Load(GridView1);
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (LEVEL < 3)
            {
                MessageBox.Show("Nu aveti permisiune!!!");
                return;
            }

            if (DeleteName.TextLength == 0 && DeleteSurname.TextLength == 0 && DeleteTel.TextLength == 0)
            {
                MessageBox.Show("Nu ati completat niciun camp!");

            }
            else
                if (Check_Nr_Tel(DeleteTel.Text) == false)
                {
                    MessageBox.Show("Nu ai introdus corect nr de tel");
                    DeleteTel.Clear();
                    return;
                }
                else
                    if (DeleteTel.TextLength != 0 && DeleteTel.TextLength != 10)
                    {
                        MessageBox.Show("Nr de tel nu are nr corespunzator de cifre");
                        DeleteTel.Clear();
                        return;
                    }
                    else
                    {
                        C.Delete(GridView1, DeleteName, DeleteSurname, DeleteTel, GridView1);
                        DeleteName.Clear();
                        DeleteSurname.Clear();
                        DeleteTel.Clear();
                    }
        }




    }
}
