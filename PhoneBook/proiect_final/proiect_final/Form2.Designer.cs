﻿namespace proiect_final
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dupa = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.NameL = new System.Windows.Forms.Label();
            this.SearchTel = new System.Windows.Forms.TextBox();
            this.SearchAdr = new System.Windows.Forms.TextBox();
            this.SearchSurname = new System.Windows.Forms.TextBox();
            this.SearchName = new System.Windows.Forms.TextBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.AddAddr = new System.Windows.Forms.TextBox();
            this.AddTel = new System.Windows.Forms.TextBox();
            this.AddSurname = new System.Windows.Forms.TextBox();
            this.AddName = new System.Windows.Forms.TextBox();
            this.AddButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.DeleteTel = new System.Windows.Forms.TextBox();
            this.DeleteSurname = new System.Windows.Forms.TextBox();
            this.DeleteName = new System.Windows.Forms.TextBox();
            this.DeleteButton = new System.Windows.Forms.Button();
            this.GridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.UpdateAdr = new System.Windows.Forms.TextBox();
            this.UpdateNrTel = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UpdateSurname = new System.Windows.Forms.TextBox();
            this.UpdateName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.load = new System.Windows.Forms.Button();
            this.dupa.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // dupa
            // 
            this.dupa.Controls.Add(this.label4);
            this.dupa.Controls.Add(this.label3);
            this.dupa.Controls.Add(this.label2);
            this.dupa.Controls.Add(this.NameL);
            this.dupa.Controls.Add(this.SearchTel);
            this.dupa.Controls.Add(this.SearchAdr);
            this.dupa.Controls.Add(this.SearchSurname);
            this.dupa.Controls.Add(this.SearchName);
            this.dupa.Location = new System.Drawing.Point(19, 36);
            this.dupa.Name = "dupa";
            this.dupa.Size = new System.Drawing.Size(285, 142);
            this.dupa.TabIndex = 54;
            this.dupa.TabStop = false;
            this.dupa.Text = " ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Nr. Tel.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Adresa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Prenume";
            // 
            // NameL
            // 
            this.NameL.AutoSize = true;
            this.NameL.Location = new System.Drawing.Point(7, 27);
            this.NameL.Name = "NameL";
            this.NameL.Size = new System.Drawing.Size(35, 13);
            this.NameL.TabIndex = 7;
            this.NameL.Text = "Nume";
            // 
            // SearchTel
            // 
            this.SearchTel.Location = new System.Drawing.Point(119, 111);
            this.SearchTel.Name = "SearchTel";
            this.SearchTel.Size = new System.Drawing.Size(145, 20);
            this.SearchTel.TabIndex = 6;
            // 
            // SearchAdr
            // 
            this.SearchAdr.Location = new System.Drawing.Point(119, 81);
            this.SearchAdr.Name = "SearchAdr";
            this.SearchAdr.Size = new System.Drawing.Size(145, 20);
            this.SearchAdr.TabIndex = 5;
            // 
            // SearchSurname
            // 
            this.SearchSurname.Location = new System.Drawing.Point(119, 51);
            this.SearchSurname.Name = "SearchSurname";
            this.SearchSurname.Size = new System.Drawing.Size(145, 20);
            this.SearchSurname.TabIndex = 4;
            // 
            // SearchName
            // 
            this.SearchName.Location = new System.Drawing.Point(119, 21);
            this.SearchName.Name = "SearchName";
            this.SearchName.Size = new System.Drawing.Size(145, 20);
            this.SearchName.TabIndex = 3;
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(21, 12);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(75, 23);
            this.SearchButton.TabIndex = 53;
            this.SearchButton.Text = "Cautare";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.AddAddr);
            this.groupBox1.Controls.Add(this.AddTel);
            this.groupBox1.Controls.Add(this.AddSurname);
            this.groupBox1.Controls.Add(this.AddName);
            this.groupBox1.Location = new System.Drawing.Point(17, 234);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(286, 121);
            this.groupBox1.TabIndex = 56;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Adresa:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 77);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Nr. tel.:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Prenume:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Nume:";
            // 
            // AddAddr
            // 
            this.AddAddr.Location = new System.Drawing.Point(120, 98);
            this.AddAddr.Name = "AddAddr";
            this.AddAddr.Size = new System.Drawing.Size(146, 20);
            this.AddAddr.TabIndex = 14;
            // 
            // AddTel
            // 
            this.AddTel.Location = new System.Drawing.Point(121, 70);
            this.AddTel.Name = "AddTel";
            this.AddTel.Size = new System.Drawing.Size(145, 20);
            this.AddTel.TabIndex = 13;
            // 
            // AddSurname
            // 
            this.AddSurname.Location = new System.Drawing.Point(121, 42);
            this.AddSurname.Name = "AddSurname";
            this.AddSurname.Size = new System.Drawing.Size(145, 20);
            this.AddSurname.TabIndex = 12;
            // 
            // AddName
            // 
            this.AddName.Location = new System.Drawing.Point(121, 14);
            this.AddName.Name = "AddName";
            this.AddName.Size = new System.Drawing.Size(145, 20);
            this.AddName.TabIndex = 11;
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(21, 212);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(75, 23);
            this.AddButton.TabIndex = 55;
            this.AddButton.Text = "Adaugare";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.DeleteTel);
            this.groupBox2.Controls.Add(this.DeleteSurname);
            this.groupBox2.Controls.Add(this.DeleteName);
            this.groupBox2.Location = new System.Drawing.Point(12, 416);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(288, 101);
            this.groupBox2.TabIndex = 58;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(7, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 24;
            this.label9.Text = "Nr. tel.:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Prenume:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 14);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Nume:";
            // 
            // DeleteTel
            // 
            this.DeleteTel.Location = new System.Drawing.Point(124, 74);
            this.DeleteTel.Name = "DeleteTel";
            this.DeleteTel.Size = new System.Drawing.Size(145, 20);
            this.DeleteTel.TabIndex = 21;
            // 
            // DeleteSurname
            // 
            this.DeleteSurname.Location = new System.Drawing.Point(124, 44);
            this.DeleteSurname.Name = "DeleteSurname";
            this.DeleteSurname.Size = new System.Drawing.Size(145, 20);
            this.DeleteSurname.TabIndex = 20;
            // 
            // DeleteName
            // 
            this.DeleteName.Location = new System.Drawing.Point(124, 14);
            this.DeleteName.Name = "DeleteName";
            this.DeleteName.Size = new System.Drawing.Size(145, 20);
            this.DeleteName.TabIndex = 19;
            // 
            // DeleteButton
            // 
            this.DeleteButton.Location = new System.Drawing.Point(19, 394);
            this.DeleteButton.Name = "DeleteButton";
            this.DeleteButton.Size = new System.Drawing.Size(75, 23);
            this.DeleteButton.TabIndex = 57;
            this.DeleteButton.Text = "Stergere";
            this.DeleteButton.UseVisualStyleBackColor = true;
            this.DeleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // GridView1
            // 
            this.GridView1.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.GridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.GridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.GridView1.Location = new System.Drawing.Point(310, 36);
            this.GridView1.Name = "GridView1";
            this.GridView1.RowHeadersWidth = 25;
            this.GridView1.Size = new System.Drawing.Size(444, 481);
            this.GridView1.TabIndex = 59;
            this.GridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridView1_CellValueChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.UpdateAdr);
            this.groupBox4.Controls.Add(this.UpdateNrTel);
            this.groupBox4.Location = new System.Drawing.Point(357, 586);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(298, 79);
            this.groupBox4.TabIndex = 65;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " ";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(26, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Adresa:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(26, 21);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 30;
            this.label14.Text = "Nr. tel.:";
            // 
            // UpdateAdr
            // 
            this.UpdateAdr.Location = new System.Drawing.Point(96, 48);
            this.UpdateAdr.Name = "UpdateAdr";
            this.UpdateAdr.Size = new System.Drawing.Size(145, 20);
            this.UpdateAdr.TabIndex = 29;
            // 
            // UpdateNrTel
            // 
            this.UpdateNrTel.Location = new System.Drawing.Point(96, 17);
            this.UpdateNrTel.Name = "UpdateNrTel";
            this.UpdateNrTel.Size = new System.Drawing.Size(145, 20);
            this.UpdateNrTel.TabIndex = 28;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.UpdateSurname);
            this.groupBox3.Controls.Add(this.UpdateName);
            this.groupBox3.Location = new System.Drawing.Point(15, 582);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(289, 83);
            this.groupBox3.TabIndex = 64;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 42;
            this.label12.Text = "Prenume:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 41;
            this.label1.Text = "Nume:";
            // 
            // UpdateSurname
            // 
            this.UpdateSurname.Location = new System.Drawing.Point(87, 48);
            this.UpdateSurname.Name = "UpdateSurname";
            this.UpdateSurname.Size = new System.Drawing.Size(141, 20);
            this.UpdateSurname.TabIndex = 40;
            // 
            // UpdateName
            // 
            this.UpdateName.Location = new System.Drawing.Point(87, 18);
            this.UpdateName.Name = "UpdateName";
            this.UpdateName.Size = new System.Drawing.Size(141, 20);
            this.UpdateName.TabIndex = 39;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(383, 570);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(58, 13);
            this.label17.TabIndex = 62;
            this.label17.Text = "Noile date:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 570);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(97, 13);
            this.label16.TabIndex = 61;
            this.label16.Text = "Informatia detinuta:";
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(24, 544);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(75, 23);
            this.UpdateButton.TabIndex = 60;
            this.UpdateButton.Text = "Update";
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // load
            // 
            this.load.Location = new System.Drawing.Point(310, 523);
            this.load.Name = "load";
            this.load.Size = new System.Drawing.Size(444, 23);
            this.load.TabIndex = 66;
            this.load.Text = "Load";
            this.load.UseVisualStyleBackColor = true;
            this.load.Click += new System.EventHandler(this.load_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(763, 746);
            this.Controls.Add(this.load);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.GridView1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.DeleteButton);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.dupa);
            this.Controls.Add(this.SearchButton);
            this.Name = "Form2";
            this.Text = "Agenda telefonica";
            this.dupa.ResumeLayout(false);
            this.dupa.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView1)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox dupa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label NameL;
        private System.Windows.Forms.TextBox SearchTel;
        private System.Windows.Forms.TextBox SearchAdr;
        private System.Windows.Forms.TextBox SearchSurname;
        private System.Windows.Forms.TextBox SearchName;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox AddAddr;
        private System.Windows.Forms.TextBox AddTel;
        private System.Windows.Forms.TextBox AddSurname;
        private System.Windows.Forms.TextBox AddName;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox DeleteTel;
        private System.Windows.Forms.TextBox DeleteSurname;
        private System.Windows.Forms.TextBox DeleteName;
        private System.Windows.Forms.Button DeleteButton;
        private System.Windows.Forms.DataGridView GridView1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox UpdateAdr;
        private System.Windows.Forms.TextBox UpdateNrTel;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox UpdateSurname;
        private System.Windows.Forms.TextBox UpdateName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button load;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label1;
    }
}