﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace proiect_final
{
    class Abonati
    {
        public int id {get; set;}
        public string Nume { get; set; }
        public string Prenume { get; set; }
        public string Adresa { get; set; }
        public string NrTel { get; set; }

        public Abonati(int i, string N, string P, string A, string NT)
        {
            id = i;
            Nume = N;
            Prenume = P;
            Adresa = A;
            NrTel = NT;
        }

    }
    
}
