﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace proiect_final
{
    public partial class Form1 : Form
    {
        Useri U;
        SqlConnection con1;

        public Form1()
        {
            InitializeComponent();
            textBox2.PasswordChar = '*';
            OpenConnection();
        }

        public void OpenConnection()
        {
            con1 = new SqlConnection("Data Source = XYZ-PC; Initial Catalog = Abonati; Integrated Security = true;");

            try
            {
                con1.Open();
                if (con1.State == ConnectionState.Open)
                    MessageBox.Show("Sunteti conectat!");
            
            }
            catch
            {
                MessageBox.Show("Nu se poate realiza conexiunea la baza de date!!!");
            }
            finally
            {
                if (con1 != null)
                    con1.Close();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (textBox1.TextLength != 0 && textBox2.TextLength != 0)
            {
                U = new Useri(textBox1.Text, textBox2.Text);
                U.VerifyUser(con1);
            }
            else
            {
                MessageBox.Show("Nu ati completat toate campurile!!!");
            }
        }
    }
}
