﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace proiect_final
{
    class Useri
    {
        int id;
        string UserName;
        string UserLevel;
        string Password;

        public Useri(string Nume, string Parola)
        {
            UserName = Nume;
            Password = Parola;
        }

        public void VerifyUser(SqlConnection con1)
        {
            try
            {
                con1.Open();

                SqlCommand comm = new SqlCommand("select Password from Useri where UserName = @Name", con1);
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@Name";
                comm.Parameters.Add(param);
                param.Value = UserName;
                string x = (string)comm.ExecuteScalar();
                if (x == Password)
                {

                    SqlCommand comm2 = new SqlCommand("select UserLevel from Useri where UserName = @Numele", con1);
                    SqlParameter param2 = new SqlParameter();
                    param2.ParameterName = "@Numele";
                    param2.Value = UserName;
                    comm2.Parameters.Add(param2);
                    UserLevel = (string)comm2.ExecuteScalar();
                    MessageBox.Show("Autentificare cu succes, " + UserName + "!" + " Ai nivel de acces " + UserLevel + ".");
                    Form2 Form2 = new Form2(UserLevel);
                    Form2.Show();

                }
                else
                    MessageBox.Show("Ati gresit userul sau parola!");             
            }
            finally
            {
                if (con1 != null)
                    con1.Close();
            }
        }

    }
}
