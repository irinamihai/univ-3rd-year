﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TemaIOC
{
    class Rule
    {
        public string Nod { get; set; }
        public string Value { get; set; }
        public bool Draw = false;
    }
}
