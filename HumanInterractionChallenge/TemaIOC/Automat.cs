﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

namespace TemaIOC
{
    interface Automata
    {
        void Load_Matrix(DataGridView DG);
        void Resolve(ListBox listBox1);
    }

    class Automat : Automata
    {

        public List<Node> Lista;
        public Dictionary<string, int> States = new Dictionary<string, int>();
        public List<string> V = new List<string>();
        public string[,] Matrix;
        string Initial_State;
        string[] Final_States;

        public Automat(string[] Alphabet, string[] States_List, string Initial, string[] Final)
        {
            Lista = new List<Node>();

            for (int i = 0; i < Alphabet.Length; i++)
                V.Add(Alphabet[i]);

            for (int i = 0; i < States_List.Length; i++)
                States.Add(States_List[i], i);

            Matrix = new string[States.Count(), V.Count()];

            Initial_State = Initial;

            Final_States = new string[Final.Length];
            //Final_States += Final;
            for (int i = 0; i < Final.Length; i++)
                Final_States[i] = Final[i];
            
        }

        public void Load_Matrix(DataGridView DG)
        {
            for (int rows = 0; rows < States.Count(); rows++)
            {
                for (int col = 0; col < V.Count(); col++)
                {
                    Matrix[rows, col] = DG.Rows[rows].Cells[col].Value.ToString();

                }
            }
        }

        private bool Find_State(string State)
        {
            foreach (Node N in Lista)
                if (N.State == State)
                    return
                        true;

            return false;
        }


        private string Get_Difference(string Old, string New)
        {
                string S = "";
                Regex r = new Regex(",");
                string[] parts = r.Split(New);
                foreach (string W in parts)
                {
                    if (Old.Contains(W) == false)
                        S = S + W + ",";
                }

                string SS = null;
                if (S != "")
                    SS = S.Remove(S.Length - 1);
                return SS;
        }

        private bool Is_New(string s)
        {
            Regex r = new Regex(",");
            string[] parts = r.Split(s);

            foreach (Node N in Lista)
            {
                int nr = 0;
                foreach (string W in parts)
                    if (N.State.Contains(W) == true)
                        nr++;

                if (nr == parts.Length)
                    return false;
            }

            return true;
        }

        private bool New_Rule(string R)
        {
            foreach (Node N in Lista)
                if (N.State == R)
                    return false;

            return true;
        }

        public void Resolve(ListBox listBox1)
        {
            try
            {
                string new_state = Initial_State; bool ok = true;
                int NR, Counter;
                ok = Find_State(new_state);

                do
                {
                    Node Nod = new Node();
                    Counter = 0; NR = 1;
                    if (Lista.Count() == 0)
                    {
                        Nod.State = new_state;

                        for (int i = 0; i < V.Count(); i++)
                        {
                            Rule R = new Rule();
                            R.Nod = Matrix[0, i];
                            R.Value = V[i];

                            Nod.T.Add(R);
                        }

                        Lista.Add(Nod);
                        Counter = 0; NR = 1;
                    }
                    else
                    {
                        for (int j = 0; j < Lista.Count(); j++)
                        {
                            NR = Lista[j].T.Count();
                            Counter = 0;
                            foreach (Rule R in Lista[j].T)
                            {
                                if (New_Rule(R.Nod) == false)
                                {
                                    Counter++;
                                    continue;
                                }
                                Node newNODE = new Node();
                                newNODE.State = R.Nod;

                                if (R.Nod.Contains(",") == true)
                                {
                                    Regex r = new Regex(",");
                                    string[] parts = r.Split(R.Nod);

                                    for (int i = 0; i < V.Count(); i++)
                                    {
                                        string newSTATE = "";
                                        for (int k = 0; k < parts.Length; k++)
                                        {
                                            string s = Get_Difference(newSTATE, Matrix[States[parts[k]], i]);
                                            if (s != "#" && k > 0 && s != null)
                                                newSTATE = newSTATE + "," + s;
                                            else
                                                if (s != "#" && k == 0 && s!=null)
                                                    newSTATE += s;

                                        }

                                        Rule newRULE = new Rule();
                                        newRULE.Nod = newSTATE;
                                        newRULE.Value = V[i];
                                        newNODE.T.Add(newRULE);
                                    }

                                    Lista.Add(newNODE);
                                }

                            }
                        }
                    }

                    ok = false;

                } while (Counter != NR);


                for (int i = 0; i < Lista.Count(); i++)
                {
                    listBox1.Items.Add(Lista[i].State);

                    for (int j = 0; j < Lista[i].T.Count(); j++)
                        listBox1.Items.Add(Lista[i].T[j].Value + "->" + Lista[i][j]);
                    
                    listBox1.Items.Add("\n");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                WriteToEventLog("Bug in Resolve function");
            }

        }

        public void Save(StreamWriter SR)
        {
            for (int i = 0; i < Lista.Count(); i++)
            {
                SR.WriteLine(Lista[i].State);
                for (int j = 0; j < Lista[i].T.Count(); j++)
                    SR.WriteLine(Lista[i].T[j].Value + "->" + Lista[i][j]);

                SR.WriteLine();
            }
        }

        public void Set_Coord(int X, int Y, int pos)
        {
            Lista[pos].Set_Coordonates(X, Y);
        }

        public int GetX(string S)
        {
            foreach (Node N in Lista)
                if (N.State == S)
                    return
                        N.x;

            return 0;
        }

        public int GetY(string S)
        {
            foreach (Node N in Lista)
                if (N.State == S)
                    return
                        N.y;

            return 0;
        }

        public bool Is_There_Line_Already(string A, string B)
        {
            for(int i = 0; i < Lista.Count(); i++)
                if(Lista[i].State == A)
                {
                    for (int j = 0; j < Lista[i].T.Count(); j++)
                        if(Lista[i].T[j].Nod == B)
                            if (Lista[i].T[j].Draw == true)
                                return true;

                    break;
                }
            return false;
        }

        public bool It_Has_Final(string S)
        {
            foreach (string F in Final_States)
                if (S.Contains(F) == true)
                    return true;

            return false;
        }

        public void WriteToEventLog(string message)
        {
            // Create an instance of EventLog
            System.Diagnostics.EventLog eventLog = new System.Diagnostics.EventLog();

            // Check if the event source exists. If not create it.
            if (!System.Diagnostics.EventLog.SourceExists("TemaIOC"))
            {
                System.Diagnostics.EventLog.CreateEventSource("TemaIOC", "Application");
            }

            // Set the source name for writing log entries.
            eventLog.Source = "TemaIOC";

            // Create an event ID to add to the event log
            int eventID = 8;

            // Write an entry to the event log.
            eventLog.WriteEntry(message,
                                System.Diagnostics.EventLogEntryType.Error,
                                eventID);

            // Close the Event Log
            eventLog.Close();
        }

    }
}