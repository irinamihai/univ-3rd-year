﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Diagnostics;
using log4net;
using log4net.Config;


namespace TemaIOC
{
    public enum Level
    {
        Low,
        Medium,
        High
    }

    public delegate void Start_Program();

    public partial class Form1 : Form
    {
        private readonly ILog log = LogManager.GetLogger(typeof(Form1));

        private System.Drawing.Drawing2D.GraphicsPath mousePath;
        private System.Drawing.Graphics g;

        public event Start_Program StartEvent;
        Automat A;
        int POS;

        public Form1()
        {
            DOMConfigurator.Configure();
            InitializeComponent();

            POS = 0;
            mousePath = new System.Drawing.Drawing2D.GraphicsPath();
            button1.Click +=new EventHandler(button1_Click);
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
               | System.Windows.Forms.AnchorStyles.Right);

            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            this.panel2.MouseClick += new MouseEventHandler(panel2_MouseClick);
            this.g = panel2.CreateGraphics();

            Alphabet.Text = Properties.Settings.Default.AlphabetContent;
            StatesAFN.Text = Properties.Settings.Default.States;
            InitialStateAFN.Text = Properties.Settings.Default.Initial;
            FinalStateAFN.Text = Properties.Settings.Default.Final;


            button3.Click +=
            delegate
            {
                listBox2.Items.Add("Introduceti alfabetul automatului");
                listBox2.Items.Add("punand virgula intre atomi si");
                listBox2.Items.Add("nelasand spatiu.");
                listBox2.Items.Add("\n");
                listBox2.Items.Add("Aceleasi reguli se aplica si");
                listBox2.Items.Add("pentru introducerea urmatoarelor");
                listBox2.Items.Add("date");
                listBox2.Items.Add("\n");
                listBox2.Items.Add("In ceea ce priveste introducerea");
                listBox2.Items.Add("tabelului pentru tranzitii,");
                listBox2.Items.Add("daca dintr-o anumita stare");
                listBox2.Items.Add("se poate ajunge in doua stari");
                listBox2.Items.Add("diferite cu aceeasi intrare,");
                listBox2.Items.Add("introduceti cele doua stari,");
                listBox2.Items.Add("separate de virgula.");

                listBox2.Items.Add("\n");
                listBox2.Items.Add("Dupa completarea datelor apasati");
                listBox2.Items.Add("butonul \"Afiseaza tabel\" pentru");
                listBox2.Items.Add("aparitia tabelului de tranzitie");
                listBox2.Items.Add("ce trebuie completat. Pentru");
                listBox2.Items.Add("rezolvare si afisarea rezultatelor");
                listBox2.Items.Add("apasati \"Incarca datele\"");
            };

            Controls.Add(button3);
           
            StartEvent += new Start_Program(Form1_StartEvent);

            StartEvent();

        }


        public void Form1_StartEvent()
        {
           
            MessageBox.Show("Cititi cu atentie toate indicatiile inainte de orice actiune!");

            log.Info("Userul a fost atentionat cu privire la cerintele completarii");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            if (Alphabet.Text.Length == 0 || StatesAFN.TextLength == 0 || InitialStateAFN.TextLength == 0 || FinalStateAFN.TextLength == 0)
            {
                log.Info("Nu a introdus toate datele");
                A.WriteToEventLog("Nu au fost completate toate datele necesare!!!");
                throw new Exception("Nu ati completat toate datele necesare!!!");
            }
               
            log.Info("Se construiesc elementele.");
            string[] Alpha = Alphabet.Text.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            //array cu starile
            string[] States = StatesAFN.Text.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string Initial_State = InitialStateAFN.Text;
            string[] Final_States = FinalStateAFN.Text.Split(new char[] { ',', ' ' }, StringSplitOptions.RemoveEmptyEntries);

            DG.ColumnCount = Alpha.Length;
            DG.RowCount = States.Length;
            
            for (int i = 0; i < States.Length; i++)
                DG.Rows[i].HeaderCell.Value = States[i];
            for (int i = 0; i < Alpha.Length; i++)
                DG.Columns[i].HeaderCell.Value = Alpha[i];

            if (DG.Rows.Count > 0)
            {
                
                for (int i = 0; i < DG.Columns.Count; i++)
                {
                    DG.Columns[i].Width = 50;
                }

            }

            if (States.Length >= 4 && Alpha.Length >= 2)
            {
                Level l = Level.High;
                maskedTextBox1.Text = l.ToString();
            }
            else
            {
                Level l = Level.Low;
                maskedTextBox1.Text = l.ToString();
            }

            A = new Automat(Alpha, States, Initial_State, Final_States);          
        }

        private void button2_Click(object sender, EventArgs e)
        {
            A.Load_Matrix(DG);

            A.Resolve(listBox1);
        }

        private void informatiiAFNAFDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProcessStartInfo sInfo = new ProcessStartInfo("http://www.mec.ac.in/resources/notes/notes/automata/nfa.htm");  
            Process.Start(sInfo);
        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            log.Info("Sfarsitul aplicatiei");

            Properties.Settings.Default.WindowLocation = this.Location;
            Properties.Settings.Default.WindowSize = this.RestoreBounds.Size;
            Properties.Settings.Default.States = StatesAFN.Text;
            Properties.Settings.Default.AlphabetContent = Alphabet.Text;
            Properties.Settings.Default.Initial = InitialStateAFN.Text;
            Properties.Settings.Default.Final = FinalStateAFN.Text;

            Properties.Settings.Default.Save();

            Application.Exit();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using( StreamWriter SR = File.AppendText("fis.txt"))
            {
                A.Save(SR);
            }
        }

        private void panel1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Perform the painting of the Panel.
           e.Graphics.DrawPath(System.Drawing.Pens.DarkRed, mousePath);
        }


        private void button4_Click(object sender, EventArgs e)
        {
           int whatever = 0;
            System.Drawing.Pen myPen = new System.Drawing.Pen(System.Drawing.Color.Red, 2);
            myPen.EndCap = LineCap.ArrowAnchor;
            myPen.StartCap = LineCap.Flat;
            myPen.CustomEndCap = new AdjustableArrowCap(4, 4);
            myPen.DashStyle = DashStyle.Solid;
            myPen.DashCap = DashCap.Triangle;


            for (int i = 0; i < A.Lista.Count(); i++ )
            {
                int p1x = A.Lista[i].x;
                int p1y = A.Lista[i].y;


                for (int j = 0; j < A.Lista[i].T.Count(); j++)
                {
                    if (whatever % 3 == 0)
                        myPen.Color = Color.Red;
                    else
                        if (whatever % 2 == 1)
                            myPen.Color = Color.SeaGreen;
                        else
                            myPen.Color = Color.Black;

                    whatever++;

                    String drawString = A.Lista[i].T[j].Value;
                    if (A.Lista[i].State != A.Lista[i].T[j].Nod)
                    {
                        bool ok = A.Is_There_Line_Already(A.Lista[i].T[j].Nod, A.Lista[i].State);
                        int p2x = A.GetX(A.Lista[i].T[j].Nod); A.Lista[i].T[j].Draw = true;
                        int p2y = A.GetY(A.Lista[i].T[j].Nod);

                        int x;
                        if (ok == true)
                            x = 10;
                        else
                            x = -10;

                        Point[] P = new Point[3];
                        P[0].X = p1x; P[0].Y = p1y;
                        P[1].X = (p1x + p2x) / 2 + x;
                        P[1].Y = (p1y + p2y) / 2 + x;
                        P[2].X = p2x; P[2].Y = p2y;

                        g.DrawCurve(myPen, P);

                        SolidBrush Brush = new SolidBrush(Color.Black);
                        Font drawFont = new Font("Arial", 12);
                        PointF Point;
                            if(x == - 10)
                                Point = new PointF((p1x + p2x) / 2 - 10, (p1y + p2y) / 2 - 10);
                            else
                                Point = new PointF((p1x + p2x) / 2 + 10, (p1y + p2y) / 2 + 10);

                            g.DrawString(drawString, drawFont, Brush, Point);
                    }
                    else
                    {
                        A.Lista[i].T[j].Draw = true;
                       
                        Point[] P = new Point[5];
                        P[0].X = p1x; P[0].Y = p1y;
                        P[1].X = p1x - 10 ; P[1].Y = p1y - 10;
                        P[2].X = p1x - 20 ; P[2].Y = p1y;
                        P[3].X = p1x - 10; P[3].Y = p1y + 10;
                        P[4].X = p1x; P[4].Y = p1y;
                        g.DrawCurve(myPen, P);

                        SolidBrush Brush = new SolidBrush(Color.Black);
                        Font drawFont = new Font("Arial", 12);
                        PointF Point = Point = new PointF(p1x - 20 , p1y + 10);
                        
                        g.DrawString(drawString, drawFont, Brush, Point);
                    }
                   
                }
            }
        }

        private void panel2_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            // Update the mouse path with the mouse information
            Point mouseDownLocation = new Point(e.X, e.Y);
            string eventString = null;

            try
            {
                switch (e.Button)
                {

                    case MouseButtons.Left:
                        {
                            eventString = "{" + A.Lista[POS].State + "}";
                            A.Lista[POS].Set_Coordonates(e.X, e.Y);
                            POS++;
                            break;
                        }
                    default:
                        break;
                }
            }
            catch(Exception ex)
                {
                    MessageBox.Show("Nu mai exista stari");
                    A.WriteToEventLog("No more states");
                    
                    log.Warn("Warning: No more states, so don't click");
                    
                }

            Point mouseDownLocation2 = new Point(e.X, e.Y - 10);
            if (eventString != null)
            {
                if (A.It_Has_Final(eventString) == false)
                    mousePath.AddString(eventString, FontFamily.GenericSerif, (int)FontStyle.Regular, 12, mouseDownLocation2, StringFormat.GenericDefault);
                else
                {
                    mousePath.AddString(eventString, FontFamily.GenericSerif, (int)FontStyle.Underline, 12, mouseDownLocation2, StringFormat.GenericDefault);
                    //log.Info("Final state added");
                }
            }
            else
            {
                
            }
            panel2.Focus();
            panel2.Invalidate();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.WindowLocation = this.Location;
            Properties.Settings.Default.WindowSize = this.RestoreBounds.Size;
            Properties.Settings.Default.States = StatesAFN.Text;
            Properties.Settings.Default.AlphabetContent = Alphabet.Text;
            Properties.Settings.Default.Initial = InitialStateAFN.Text;
            Properties.Settings.Default.Final = FinalStateAFN.Text;

            Properties.Settings.Default.Save();
        }  
    }
}
