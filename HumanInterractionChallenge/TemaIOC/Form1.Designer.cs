﻿namespace TemaIOC
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
       /*private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        */
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Alfabet = new System.Windows.Forms.Label();
            this.Stari = new System.Windows.Forms.Label();
            this.Stare_Initiala = new System.Windows.Forms.Label();
            this.Stare_Finala = new System.Windows.Forms.Label();
            this.Alphabet = new System.Windows.Forms.TextBox();
            this.StatesAFN = new System.Windows.Forms.TextBox();
            this.InitialStateAFN = new System.Windows.Forms.TextBox();
            this.FinalStateAFN = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.DG = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.maskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optiuniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.informatiiAFNAFDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button4 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.DG)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Alfabet
            // 
            this.Alfabet.AutoSize = true;
            this.Alfabet.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Alfabet.Location = new System.Drawing.Point(24, 54);
            this.Alfabet.Name = "Alfabet";
            this.Alfabet.Size = new System.Drawing.Size(67, 13);
            this.Alfabet.TabIndex = 0;
            this.Alfabet.Text = "Alfabet AFN:";
            // 
            // Stari
            // 
            this.Stari.AutoSize = true;
            this.Stari.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Stari.Location = new System.Drawing.Point(24, 91);
            this.Stari.Name = "Stari";
            this.Stari.Size = new System.Drawing.Size(112, 13);
            this.Stari.TabIndex = 1;
            this.Stari.Text = "Multimea starilor AFN: ";
            // 
            // Stare_Initiala
            // 
            this.Stare_Initiala.AutoSize = true;
            this.Stare_Initiala.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Stare_Initiala.Location = new System.Drawing.Point(24, 126);
            this.Stare_Initiala.Name = "Stare_Initiala";
            this.Stare_Initiala.Size = new System.Drawing.Size(106, 13);
            this.Stare_Initiala.TabIndex = 2;
            this.Stare_Initiala.Text = "Starea initiala a AFN:";
            // 
            // Stare_Finala
            // 
            this.Stare_Finala.AutoSize = true;
            this.Stare_Finala.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Stare_Finala.Location = new System.Drawing.Point(24, 161);
            this.Stare_Finala.Name = "Stare_Finala";
            this.Stare_Finala.Size = new System.Drawing.Size(108, 13);
            this.Stare_Finala.TabIndex = 3;
            this.Stare_Finala.Text = "Starile finale ale AFN:";
            // 
            // Alphabet
            // 
            this.Alphabet.Location = new System.Drawing.Point(174, 54);
            this.Alphabet.Name = "Alphabet";
            this.Alphabet.Size = new System.Drawing.Size(389, 20);
            this.Alphabet.TabIndex = 4;
            // 
            // StatesAFN
            // 
            this.StatesAFN.Location = new System.Drawing.Point(174, 91);
            this.StatesAFN.Name = "StatesAFN";
            this.StatesAFN.Size = new System.Drawing.Size(389, 20);
            this.StatesAFN.TabIndex = 5;
            // 
            // InitialStateAFN
            // 
            this.InitialStateAFN.Location = new System.Drawing.Point(174, 123);
            this.InitialStateAFN.Name = "InitialStateAFN";
            this.InitialStateAFN.Size = new System.Drawing.Size(389, 20);
            this.InitialStateAFN.TabIndex = 6;
            // 
            // FinalStateAFN
            // 
            this.FinalStateAFN.Location = new System.Drawing.Point(174, 161);
            this.FinalStateAFN.Name = "FinalStateAFN";
            this.FinalStateAFN.Size = new System.Drawing.Size(389, 20);
            this.FinalStateAFN.TabIndex = 7;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Location = new System.Drawing.Point(623, 51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(153, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "Afiseaza tabel";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // DG
            // 
            this.DG.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.DG.ColumnHeadersHeight = 20;
            this.DG.Location = new System.Drawing.Point(12, 187);
            this.DG.Name = "DG";
            this.DG.RowHeadersWidth = 50;
            this.DG.Size = new System.Drawing.Size(277, 316);
            this.DG.TabIndex = 9;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button2.Location = new System.Drawing.Point(623, 85);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(153, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Incarca datele";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(378, 187);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(185, 316);
            this.listBox1.TabIndex = 11;
            // 
            // maskedTextBox1
            // 
            this.maskedTextBox1.Location = new System.Drawing.Point(701, 133);
            this.maskedTextBox1.Name = "maskedTextBox1";
            this.maskedTextBox1.Size = new System.Drawing.Size(75, 20);
            this.maskedTextBox1.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(738, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Level";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button3.Location = new System.Drawing.Point(623, 223);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(153, 23);
            this.button3.TabIndex = 14;
            this.button3.Text = "Reguli de completare";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // listBox2
            // 
            this.listBox2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(594, 252);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(204, 251);
            this.listBox2.TabIndex = 15;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optiuniToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip1.Size = new System.Drawing.Size(1309, 24);
            this.menuStrip1.TabIndex = 16;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optiuniToolStripMenuItem
            // 
            this.optiuniToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.informatiiAFNAFDToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.toolStripMenuItem1,
            this.quitToolStripMenuItem});
            this.optiuniToolStripMenuItem.Name = "optiuniToolStripMenuItem";
            this.optiuniToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.optiuniToolStripMenuItem.Text = "Optiuni";
            // 
            // informatiiAFNAFDToolStripMenuItem
            // 
            this.informatiiAFNAFDToolStripMenuItem.Name = "informatiiAFNAFDToolStripMenuItem";
            this.informatiiAFNAFDToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.informatiiAFNAFDToolStripMenuItem.Text = "Informatii AFN -> AFD";
            this.informatiiAFNAFDToolStripMenuItem.Click += new System.EventHandler(this.informatiiAFNAFDToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(190, 6);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.button4.Location = new System.Drawing.Point(701, 173);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 17;
            this.button4.Text = "Deseneaza";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel2.Location = new System.Drawing.Point(894, 54);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(403, 455);
            this.panel2.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1309, 521);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.maskedTextBox1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.DG);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.FinalStateAFN);
            this.Controls.Add(this.InitialStateAFN);
            this.Controls.Add(this.StatesAFN);
            this.Controls.Add(this.Alphabet);
            this.Controls.Add(this.Stare_Finala);
            this.Controls.Add(this.Stare_Initiala);
            this.Controls.Add(this.Stari);
            this.Controls.Add(this.Alfabet);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.DG)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Alfabet;
        private System.Windows.Forms.Label Stari;
        private System.Windows.Forms.Label Stare_Initiala;
        private System.Windows.Forms.Label Stare_Finala;
        private System.Windows.Forms.TextBox Alphabet;
        private System.Windows.Forms.TextBox StatesAFN;
        private System.Windows.Forms.TextBox InitialStateAFN;
        private System.Windows.Forms.TextBox FinalStateAFN;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView DG;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.MaskedTextBox maskedTextBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optiuniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem informatiiAFNAFDToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel2;
    }
}

