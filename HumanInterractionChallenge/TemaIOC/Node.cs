﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TemaIOC
{
    class Node
    {
        public string State { get; set; }
        public List<Rule> T = new List<Rule>();
       // public int Contor;
        public int x, y;
        
        public string this[int pos]
        {
            get
            {
                return T[pos].Nod;
            }
            set
            {
                T[pos].Nod = value;
            }
        }

        public void Set_Coordonates(int X, int Y)
        {
            x = X;
            y = Y;
        }

        /*
        public int this[int poz]
        {
            get
            {
                return T[poz].Value;
            }
            set
            {
                T[poz].Value = value;
            }
        }    */   
    }
}
