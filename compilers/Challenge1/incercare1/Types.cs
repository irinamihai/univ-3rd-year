﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;


namespace incercare1
{
    class Types
    {   
        public List<string> punctuatie = new List<string>();
        public List<string> caractere = new List<string>();
        public List<string> operatori = new List<string>();
        public List<string> cifra = new List<string>();
        public Dictionary<int, string> Terminale;

        public Types()
        {
            string[] semn = { "{", "}", "]", "[", ")", "(", ",", ";", ":", "\t"};
            foreach (string x in semn)
                    punctuatie.Add(x);


            for (int i = 65; i <= 90; i++)
            {
                char l = (char)i;
                string s = "" + l + "";
                caractere.Add(s);
            }
            for (int i = 97; i <= 122; i++)
            {
                char l = (char)i;
                string s = "" + l + "";
                caractere.Add(s);
            }
                caractere.Add("#");
                caractere.Add("_");

                for (int i = 48; i <= 57; i++)
                {
                    char l = (char)i;
                    string s = "" + l + "";
                    cifra.Add(s);
                }

            string[] op = { ">", "<", "=", "+", "-", "%", "&", "|", "^", "!", "~", "==", "!=", "<=", ">=", "+=", "-=", "*=", "/=", "%=", "&=", "++", "--" };
            foreach (string x in op)
                operatori.Add(x);

            Terminale = new Dictionary<int, string>();

            Terminale.Add(0, "Punctuation");
            Terminale.Add(1, "Identifier");
            Terminale.Add(2, "Constant");
            Terminale.Add(3, "RealNumber");
            Terminale.Add(4, "Operator");
            Terminale.Add(8, "Comment on multiple lines");
            Terminale.Add(10, "Comment in single line");
            Terminale.Add(13, "StringConstant");
            Terminale.Add(17, "CharConstant");
            Terminale.Add(14, "Error");

        }

        public string SearchType(string L)
        {
                if (punctuatie.Exists(element => element == L))
                        return "punctuatie";
            
                if (caractere.Exists(element => element == L))
                        return "caracter";

                if (cifra.Exists(element => element == L))
                        return "cifra";

                if (operatori.Exists(element => element == L))
                        return "operator";

                if (L == ".")
                    return "punct";

                if(L == "*")
                    return "steluta";

                if (L == "/")
                    return "slash";

                if (L == "\n" || L=="\r" )
                    return "endline";

                if (L == " ")
                    return "space";

                if (L == "\"")
                    return "ghilimele";

                if (L == "'")
                    return "apostrof";

                if (L == "\\")
                    return "backslash";

                if (L == "$")
                    return "dolar";

                return null;
        }

    }
}