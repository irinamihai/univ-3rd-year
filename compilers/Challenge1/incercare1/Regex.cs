﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace incercare1
{
    class RegEx
    {
        keywords K;
        public List<string> Identifiers;
        public List<string> Constants;

        Rules R;

        public RegEx()
        { 
            K = new keywords();
            Identifiers = new List<string>();
            Constants = new List<string>();
            R = new Rules();
        }

        public void Transmit_Data(string type, string value, ref ListBox Altele, int nr)
        {
            if(type == "Error")
                Altele.Items.Add(type + " " + value + " on line " + nr);
            else
                Altele.Items.Add(type + " " + value);

        }

        public int Find_Line(int index, string text)
        { 
            int nr = 0;
            for (int i = 0; i < index; i++)
                if (text[i] == '\n')
                    nr++;

            return nr;
        }

        public void Verify_Constant(string value, string type, ref ListBox Altele,int nr)
        {
            if (value.Length >= 20)
                Transmit_Data("Error", value, ref Altele, nr);
            else
                Constants.Add(value);
        }

        public void Apply_Regex(string reg, ref string text, string type, ref ListBox Altele)
        {
            Regex rgx = new Regex(reg);
            int NR = 0;

            foreach (Match match in rgx.Matches(text))
            {
                if (type == "Identifier")
                {
                    if (K.verify_keyword(match.Value) == true)
                        Transmit_Data("Keyword", match.Value, ref Altele, NR);
                    else
                        Identifiers.Add(match.Value);
                }
                else
                    if (type == "Constant")
                        Verify_Constant(match.Value, type, ref Altele, NR);
                    else
                        if (type == "Char constant")
                        {
                            if (match.Value.Length > 3)
                            {
                                NR = Find_Line(match.Index, text);
                                Transmit_Data("Error", match.Value, ref Altele, NR + 1); NR = 0;
                            }
                        }
                        else
                            if (type == "Error")
                            {
                                NR = Find_Line(match.Index, text);
                                Transmit_Data(type, "$", ref Altele, NR + 1); NR = 0;
                            }
                            else
                                Transmit_Data(type, match.Value, ref Altele, NR);                                
            }
            text = Regex.Replace(text, reg, " ");
        }

        public void Analiza(ref ListBox Altele, string PATH)
        {
            StreamReader streamReader = new StreamReader(PATH);
            string text = streamReader.ReadToEnd();
            text += " ";

            Altele.Items.Add("\n\n---------------------------------" + "All" + "---------------------------------\n\n");

            foreach (string key in R.Rules_List.Keys)
            {
                Apply_Regex(key, ref text, R.Rules_List[key], ref Altele);
            }           
        }
    }
}
