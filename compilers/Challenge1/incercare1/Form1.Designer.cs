﻿namespace incercare1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonAutomat = new System.Windows.Forms.Button();
            this.Speciale = new System.Windows.Forms.ListBox();
            this.Altele = new System.Windows.Forms.ListBox();
            this.Button_Regex = new System.Windows.Forms.Button();
            this.Code = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonAutomat
            // 
            this.ButtonAutomat.Location = new System.Drawing.Point(169, 12);
            this.ButtonAutomat.Name = "ButtonAutomat";
            this.ButtonAutomat.Size = new System.Drawing.Size(128, 23);
            this.ButtonAutomat.TabIndex = 0;
            this.ButtonAutomat.Text = "Automat Finit";
            this.ButtonAutomat.UseVisualStyleBackColor = true;
            // 
            // Speciale
            // 
            this.Speciale.FormattingEnabled = true;
            this.Speciale.HorizontalScrollbar = true;
            this.Speciale.Location = new System.Drawing.Point(379, 19);
            this.Speciale.Name = "Speciale";
            this.Speciale.Size = new System.Drawing.Size(224, 446);
            this.Speciale.TabIndex = 1;
            // 
            // Altele
            // 
            this.Altele.FormattingEnabled = true;
            this.Altele.HorizontalScrollbar = true;
            this.Altele.Location = new System.Drawing.Point(613, 18);
            this.Altele.Name = "Altele";
            this.Altele.Size = new System.Drawing.Size(220, 446);
            this.Altele.TabIndex = 2;
            // 
            // Button_Regex
            // 
            this.Button_Regex.Location = new System.Drawing.Point(169, 73);
            this.Button_Regex.Name = "Button_Regex";
            this.Button_Regex.Size = new System.Drawing.Size(128, 23);
            this.Button_Regex.TabIndex = 3;
            this.Button_Regex.Text = "Regular Expressions";
            this.Button_Regex.UseVisualStyleBackColor = true;
            this.Button_Regex.Click += new System.EventHandler(this.Button_Regex_Click);
            // 
            // Code
            // 
            this.Code.FormattingEnabled = true;
            this.Code.Location = new System.Drawing.Point(12, 127);
            this.Code.Name = "Code";
            this.Code.Size = new System.Drawing.Size(310, 342);
            this.Code.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(13, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Load source file";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 98);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "Clear Code";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 481);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Code);
            this.Controls.Add(this.Button_Regex);
            this.Controls.Add(this.Altele);
            this.Controls.Add(this.Speciale);
            this.Controls.Add(this.ButtonAutomat);
            this.Name = "Form1";
            this.Text = "Analizor Lexical";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonAutomat;
        private System.Windows.Forms.ListBox Speciale;
        private System.Windows.Forms.ListBox Altele;
        private System.Windows.Forms.Button Button_Regex;
        private System.Windows.Forms.ListBox Code;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

