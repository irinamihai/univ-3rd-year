﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace incercare1
{
    class keywords
    {
        List<string> KeyWord = new List<string>();

        public keywords()
        {
            using (StreamReader sr = new StreamReader("keywords.txt"))
            {
                string s;
                Regex r = new Regex(" +");
                while ((s = sr.ReadLine()) != null)
                {
                    string[] parts = r.Split(s);

                    foreach (String W in parts)
                        KeyWord.Add(W);

                }

                int nr = KeyWord.Count();
                //MessageBox.Show("Nr: " + nr);
            }
        }

        public bool verify_keyword(string word)
        {
            if (KeyWord.Exists(element => element == word))
                return true;
            else
                return false;
        }

    }
}
