﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace incercare1
{
    class Tokens
    {
        public List<string> Identifiers;
        public List<string> Constants;
        public List<string> KeyWords;
        //public List<KeyValuePair<string, string>> All = new List<KeyValuePair<string, string>>();

        public Tokens()
        { 
            Identifiers = new List<string>();
            Constants = new List<string>();
            KeyWords = new List<string>();
 
        }

        public void Transmit_Data(string type, string value, ref ListBox Altele, int linecount)
        {
            if(type == "Error")
                Altele.Items.Add(type + " " + value + " on line " + linecount);
            else
                Altele.Items.Add(type + " " + value);
        }

        public void Add_Token(string word, string type, ref ListBox Altele, int linecount)
        {
            switch (type)
            {
                case "Punctuation": Transmit_Data(type, word, ref Altele,linecount);
                                    break;
                case "Identifier": Identifiers.Add(word);
                                    Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "KeyWord": //KeyWords.Add(word);
                                    Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "Constant": Constants.Add(word);
                                    Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "RealNumber": Constants.Add(word);
                                    Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "Operator": Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "Comment on multiple lines": Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "Comment in single line": Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "StringConstant": Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "CharConstant": Transmit_Data(type, word, ref Altele, linecount);
                                    break;
                case "Error": Transmit_Data(type, word, ref Altele, linecount);
                                    break;
            }
        }            
  }
}

