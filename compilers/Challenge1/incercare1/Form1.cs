﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;


namespace incercare1
{

    public partial class Form1 : Form
    {
        FiniteAutomata FA = new FiniteAutomata();
        RegEx RE = new RegEx();
        string PATH;

        public Form1()
        {
            InitializeComponent();


            ButtonAutomat.Click +=new EventHandler(ButtonAutomat_Click);

            //Button_Regex.Click +=new EventHandler(Button_Regex_Click);
        }

        
            public void Get_it_Out()
            {
                Speciale.Items.Clear();
                Speciale.Refresh();

                Speciale.Items.Add("\n---------------------------Identifiers---------------------------\n\n");
                foreach (object o in FA.T.Identifiers)               
                {
                    Speciale.Items.Add(o.ToString());
                    //Speciale.Items.Add("\n");
                }

                Speciale.Items.Add("\n\n--------------------------Constants---------------------------\n");
                foreach (object o in FA.T.Constants)               
                {
                    Speciale.Items.Add(o.ToString());
                    //Speciale.Items.Add("\n");
                }
            }

            public void Get_it_Out2()
            {

                Speciale.Items.Clear();
                Speciale.Items.Add("\n---------------------------Identifiers---------------------------\n\n");
                foreach (object o in RE.Identifiers)
                {
                    Speciale.Items.Add(o.ToString());
                    //Speciale.Items.Add("\n");
                }

                Speciale.Items.Add("\n\n---------------------------Constants---------------------------\n");
                foreach (object o in RE.Constants)
                {
                    Speciale.Items.Add(o.ToString());
                    //Speciale.Items.Add("\n");
                }
            }

        private void ButtonAutomat_Click(object sender, EventArgs e)
        {
            Altele.Items.Clear();
            Speciale.Items.Clear();
            FA.Analiza(ref Altele, PATH);

            Get_it_Out();
            FA.T.Identifiers.Clear();
   
            FA.T.Constants.Clear();

        }
       
        private void Button_Regex_Click(object sender, EventArgs e)
        {
            Speciale.Items.Clear();
            Altele.Items.Clear();

            RE.Analiza(ref Altele, PATH);
           
            Get_it_Out2();

            RE.Identifiers.Clear();
            RE.Constants.Clear();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                       // string tempPath = openFileDialog1.;

                        using (myStream)
                        {
                            string dirName =System.IO.Path.GetDirectoryName(openFileDialog1.FileName);
                           
                            PATH = openFileDialog1.FileName;
                            
                            string s;
                            StreamReader reader = new StreamReader(myStream);
                            while ((s = reader.ReadLine()) != null)
                                Code.Items.Add(s);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Code.Items.Clear();
        }
    }
}
