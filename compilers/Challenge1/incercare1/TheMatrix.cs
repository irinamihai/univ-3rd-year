﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace incercare1
{
    class TheMatrix
    {
        public int[,] Matrix;
        public Dictionary<string, int> Dictionary;

        public TheMatrix()
        {
            Matrix = new int[18, 12];
            string fileContent = File.ReadAllText("TheMatrix2.txt");

            string[] integerStrings = fileContent.Split(new char[] { ' ', '\t', '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            int[] integers = new int[integerStrings.Length];

            int nr = 0;
            for (int i = 0; i < 18; i++)
                for (int j = 0; j < 12; j++)
                {
                    Matrix[i,j] = int.Parse(integerStrings[nr]);
                    nr++;

                }

            Dictionary = new Dictionary<string, int>();

            Dictionary.Add("caracter", 0);
            Dictionary.Add("cifra", 1);
            Dictionary.Add("operator", 2);
            Dictionary.Add("punctuatie", 3);
            Dictionary.Add("space", 3);
            Dictionary.Add("steluta", 4);
            Dictionary.Add("ghilimele", 5);
            Dictionary.Add("apostrof", 6);
            Dictionary.Add("slash", 7);
            Dictionary.Add("backslash", 8);
            Dictionary.Add("punct", 9);
            Dictionary.Add("endline", 10);
            Dictionary.Add("dolar", 11);
        }

    }
}

