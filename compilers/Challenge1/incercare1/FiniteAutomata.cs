﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace incercare1
{
    class FiniteAutomata
    {
        Types Tipuri;
        TheMatrix M;
        keywords K;
        public Tokens T;
        string type, name;

        public FiniteAutomata()
        {
            Tipuri = new Types();
            M = new TheMatrix();
            K = new keywords();
            T = new Tokens();
        }

       

        public bool Is_KeyWord(string word, int state)
        {
            return ( K.verify_keyword(word));
            
        }
        

        public void Verify_Type(string word, string text, int nr, int state, ref ListBox Altele, int linecount)
        {        
                if (nr == text.Length - 1 && state == 6)
                    T.Add_Token(word, Tipuri.Terminale[14], ref Altele, linecount);
                else
                    if (state == 1)
                        if (Is_KeyWord(word, state))
                            T.Add_Token(word, "KeyWord", ref Altele, linecount);
                        else
                            T.Add_Token(word, Tipuri.Terminale[state], ref Altele, linecount);
                    else
                        T.Add_Token(word, Tipuri.Terminale[state], ref Altele, linecount);
        }

        public void Verify_State2(string text, ref int next_state, ref int nr, int state, string tip2, string word)
        {
            string s2 = "" + text[nr + 2] + "";
            string tip = Tipuri.SearchType(s2);
            if (tip == "cifra")
            {
                next_state = M.Matrix[state, M.Dictionary[tip2]];
                nr++;
            }
            else
            {
                if (word.Length >= 20)
                    next_state = 14;
                else
                {
                    next_state = 0;
                    nr++;
                }
            }
        }


        public void Verify_state2_max(string text, ref int next_state, ref int nr, int state, string tip2, string word)
        {
            if (word.Length >= 20)
                state = 14;
            else return;
        
        }

        public void Verify_State4(string text, ref int nr, string word, ref int next_state)
        {
            string s2 = "" + text[nr + 1] + "";
            string tip = Tipuri.SearchType(s2);
            string s3 = word + s2;
            if (tip == "operator" && Tipuri.SearchType(s3) == "operator")
            {
                word += s2;
                next_state = 0;
                nr += 2;
            }
            else
            {
                next_state = 0;
                nr++;
            }
        }

        public void Verify_Next_State(ref int nr, string text, ref string word, ref string s1, ref int next_state, ref int state)
        {
            if (nr == text.Length - 1)
                return;
            word += s1;
            state = next_state;
            s1 = "" + text[nr + 1] + "";
            string tip2 = Tipuri.SearchType(s1);

                if (state == 2 && tip2 == "punct")
                    Verify_State2(text, ref next_state, ref nr, state, tip2, word);
                else
                    if (state == 4)
                        Verify_State4(text, ref nr, word, ref next_state);
                    else
                    {
                        if (state == 2 && word.Length >= 20)
                            state = 14;
                        else
                        {
                            next_state = M.Matrix[state, M.Dictionary[tip2]];
                            nr++;
                        }
                    }
            if (state == 14)
                next_state = 0; 
        }

        public void Analiza(ref ListBox Altele, string PATH)
        {
            Altele.Items.Add("\n\n---------------------------------" + "All" + "---------------------------------");

            StreamReader streamReader = new StreamReader(PATH);
            string text = streamReader.ReadToEnd();
            text += " ";
            string word = "";
            int nr = 0, state = 0, linecount = 1;
           
            while(nr < text.Length - 1)
            {
                string s1 = "" + text[nr] + "";
                string tip1 = Tipuri.SearchType(s1);
                    
                int  next_state = M.Matrix[state, M.Dictionary[tip1]];

                if (next_state == 0 && tip1 != "endline" && tip1 != "space")
                {
                    word += s1;
                    T.Add_Token(word, Tipuri.Terminale[state], ref Altele, linecount);
                    word = "";
                    nr++;
                    continue;
                }
                else
                    if (next_state == 0 && (tip1 == "endline" || tip1 == "space"))
                    {
                        if (s1 == "\n")
                            linecount++;
                        nr++;
                        continue;
                    }
                
                while (next_state != 0)
                    Verify_Next_State(ref nr, text, ref word, ref s1, ref next_state, ref state);

                Verify_Type(word, text, nr, state, ref Altele, linecount);

                state = 0;
                word = "";                
            }
            streamReader.Close();
        }
    }
}
