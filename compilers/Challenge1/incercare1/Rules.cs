﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace incercare1
{
    class Rules
    {
        public Dictionary <string, string> Rules_List;

        public Rules()
        {
            Rules_List = new Dictionary<string, string>();

            Rules_List.Add(@"(/\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*+/)|(//.*)", "Comment");
            Rules_List.Add("'(.)'|'(.)*'", "Char constant");
            Rules_List.Add("\"([^\"]|(\\\\\"))*[^\\\\]\"", "String constant");
            Rules_List.Add("[a-zA-Z_#][a-zA-Z_0-9]*", "Identifier");
            Rules_List.Add(@"[+-]?[0-9][0-9]*\.?([0-9]*)?", "Constant");
            Rules_List.Add(@"(\+){1,2}|-{1,2}|(\*)|(\\)|%|&{1,2}|(\|){1,2}|^|={1,2}|!|~|(\?)|[+\-&|><!*\\%]=|(>>)|(<<)", "Operator");
            Rules_List.Add(@"[\.,{}():;\[\]]", "Punctuation");
            Rules_List.Add("[$]", "Error");
        }
    }
}
