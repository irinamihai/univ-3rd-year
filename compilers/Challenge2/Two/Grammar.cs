﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.CSharp;
using System.Diagnostics;
using System.Globalization;
using System.CodeDom.Compiler;

namespace Two
{
    class Grammar
    {
        public string S {get; set;}
        List<string> Terminale = new List<string>();
        List<string> Neterminale = new List<string>();
        public List<KeyValuePair<string, List<string>>> RulesList = new List<KeyValuePair<string, List<string>>>(10);
        List<string> FF = new List<string>();
        int R = 0;
        string[,] Matrix;

        struct Sets
        {
            public string N;
            public List<string> D;
        }

        Sets []Seturi;
        
        public void Add_Terminal(string s)
        {
            Terminale.Add(s);
        }

        public void Add_Neterminal(string s)
        {
            Neterminale.Add(s);
        }

        public void Add_Rule(string s, List<string> l)
        {
            RulesList.Add(new KeyValuePair<string, List<string>>(s, new List<string>(l)));
            R++;
        }

        public void Print_Rules(ListBox listbox1)
        {
            foreach (KeyValuePair<string, List<string>> element in RulesList)
            {
                string s="";
                foreach (String x in element.Value)
                {
                    s += x+" ";
                }
                listbox1.Items.Add(element.Key+" -> "+s);
            }

            for (int i = 0; i < Terminale.Count(); i++)
                listbox1.Items.Add(Terminale[i]);


            for (int i = 0; i < Neterminale.Count(); i++)
                listbox1.Items.Add(Neterminale[i]);

            Seturi = new Sets[R];
        }

        public void First_Follow()
        {
            int nr = 0;
            List<string> EVALUATE = new List<string>();
            foreach(KeyValuePair<string, List<string>> element in RulesList)
            {
                if (element.Value[0] != "#")
                {
                   // MessageBox.Show("Pentru " + element.Value[0]);
                    Find_First(element.Value[0]);
                    Add_To_Set(element.Key, FF, nr);  
                }
                else
                {
                  //  MessageBox.Show("Pentru " + element.Key);
                    EVALUATE.Add(element.Key);
                    Find_Follow(element.Key, EVALUATE);
                    Add_To_Set(element.Key, FF, nr);
                    EVALUATE.Clear();
                }
      
                FF.Clear();
                Matrix = new string[Terminale.Count()+Neterminale.Count() + 1, Terminale.Count() + 1];
            
                nr++;
            }
        }

        public void Find_First(string N)
        {
            
           if (Terminale.Contains(N) == true)
           {
               if (FF.Contains(N) == false)
                   FF.Add(N);
           }
            else
            {
                foreach (KeyValuePair<string, List<string>> element in RulesList)
                    if (element.Key == N)
                    {
                        string x = element.Value[0];
                        if (Terminale.Contains(element.Value[0]) == true)
                        {
                            if (FF.Contains(x) == false)
                                FF.Add(x);
                        }
                        else
                            if (Neterminale.Contains(element.Value[0]) == true)
                                Find_First(element.Value[0]);
                    }
            }
        }

        public void Find_First_For_Follow(string N)
        {
            if (Terminale.Contains(N) == true)
            {
                if (FF.Contains(N) == false)
                    FF.Add(N);
            }
            else
            {
                foreach (KeyValuePair<string, List<string>> element in RulesList)
                    if (element.Key == N)
                    {
                        string x = element.Value[0];
                        if (Terminale.Contains(element.Value[0]) == true)
                        {
                            if (FF.Contains(x) == false)
                                FF.Add(x);
                        }
                        else
                            if (Neterminale.Contains(element.Value[0]) == true)
                                Find_First_For_Follow(element.Value[0]);
                    }
            }
        }

        bool Reaches_Epsilon(string N)
        {
            foreach (KeyValuePair<string, List<string>> element in RulesList)
            {
                if (element.Key == N)
                    if (element.Value[0] == "#")
                        return true;
            }

            return false;
        }

        public void Find_Follow(string N, List<string> EVALUATE)
        {
            int poz;

            if (N == Neterminale[0])
                FF.Add("$");

            foreach (KeyValuePair<string, List<string>> element in RulesList)
            {
                if (element.Value.Contains(N) == true)
                {
                    poz = element.Value.IndexOf(N);

                    if (poz != element.Value.Count() - 1)
                    {
                        string x = element.Value[poz + 1];
                        if (x != element.Key)
                        {
                            Find_First_For_Follow(x);

                            if (Reaches_Epsilon(x) == true)
                            {
                                if (EVALUATE.Contains(element.Key) == false)
                                {
                                    EVALUATE.Add(element.Key);
                                    Find_Follow(element.Key, EVALUATE);
                                }
                                else
                                    continue;
                            }
                        }
                        //return;
                    }
                    else
                        if (N == element.Key)
                            continue;
                        else
                        {
                            if (EVALUATE.Contains(element.Key) == false)
                            {
                                EVALUATE.Add(element.Key);
                                Find_Follow(element.Key, EVALUATE);
                            }
                            else
                                continue;
                        }
                }
            }
        }

        public void Add_To_Set(string V, List<string> F, int nr)
        {
            Seturi[nr] = new Sets();
            Seturi[nr].N = V;
            Seturi[nr].D = new List<string>(F);
        }

        public void Print_First_Follow(ListBox listbox2)
        {
            for (int i = 0; i < R; i++)
            {
                string s = "";
                s += Seturi[i].N + " -> ";

                foreach (String S in Seturi[i].D)
                {
                    s += S;
                    s += ", ";
                }
                listbox2.Items.Add(s);
            }
        }

        public void Fill_Matrix(DataGridView dataGridView1)
        {
            dataGridView1.ColumnCount = Terminale.Count() + 1;

            int e = Terminale.IndexOf("+"); 
            for (int i = 0; i < Neterminale.Count(); i++)
                for (int j = 0; j < Seturi.Count(); j++)
                {
                    if(Neterminale[i] == Seturi[j].N)
                        foreach (string k in Seturi[j].D)
                        {
                            int x = j + 1;
                            if (k != "$")
                            {
                                int poz = Terminale.IndexOf(k);
                                Matrix[i, poz] = Convert.ToString(x);
                            }
                            else
                                Matrix[i, Terminale.Count()] = Convert.ToString(x);
                        }
                }
            int nr = 0;
            for (int i = Neterminale.Count(); i < Terminale.Count() + Neterminale.Count(); i++)
                Matrix[i, nr++] = "P";

            Matrix[Neterminale.Count() + Terminale.Count(), Terminale.Count()] = "A";
        
                for (int row = 0; row < Terminale.Count() + Neterminale.Count() + 1; row++)
                {
                    string[] currentColumn = new string[Terminale.Count()+1];
                        for (int col = 0; col < Terminale.Count() + 1; col++)

                        currentColumn[col] = Matrix[row,col];
                    
                    dataGridView1.Rows.Add(currentColumn);
                }

                for (int i = 0; i < Neterminale.Count(); i++)
                    dataGridView1.Rows[i].HeaderCell.Value = Neterminale[i];
            
                for(int i = Neterminale.Count(); i < Neterminale.Count() + Terminale.Count(); i++)
                    dataGridView1.Rows[i].HeaderCell.Value = Terminale[i - Neterminale.Count()];

                dataGridView1.Rows[Terminale.Count() + Neterminale.Count()].HeaderCell.Value = "$";
                for (int i = 0; i < Terminale.Count(); i++)
                    dataGridView1.Columns[i].HeaderCell.Value = Terminale[i];

                dataGridView1.Columns[Terminale.Count()].HeaderCell.Value = "$";
        }

        public void Check_Grammar1()
        {
            for (int j = 0; j < Neterminale.Count(); j++ )
            {
                int nr = 0;
                List<string> Common = new List<string>(Check_Common(Neterminale[j]));
                string NEW_T = "";

                if (Common.Count() == 0)
                    continue;

                for (int i = 0; i < RulesList.Count(); i++)
                {
                    List<string> L;
                    if (RulesList[i].Key == Neterminale[j] && i != j)
                    {
                        if (Check_if_Common(RulesList[i].Value, Common) == true)
                        {
                            if (nr == 0)
                            {
                                nr++;

                                string[] V = new string[RulesList[i].Value.Count() - Common.Count()];

                                //partea necomuna
                                RulesList[i].Value.CopyTo(Common.Count(), V, 0, RulesList[i].Value.Count() - Common.Count());

                                //ramane doar partea comuna
                                RulesList[i].Value.RemoveRange(Common.Count(), RulesList[i].Value.Count() - Common.Count());
                                NEW_T = RulesList[i].Key + "1";
                                RulesList[i].Value.Add(NEW_T);

                                int nnn = V.Length;
                                Add_Neterminal(NEW_T);
                                L = new List<string>();
                                L.AddRange(V);

                                if (nnn == 0)
                                    L.Add("#");

                                Add_Rule(NEW_T, L);
                            }
                            else
                            {
                                string[] V = new string[RulesList[i].Value.Count() - Common.Count()];                               
                                //parea necomuna
                                RulesList[i].Value.RemoveRange(0, Common.Count());
                                List<string> AL = new List<string>(RulesList[i].Value);
                                if (V.Length == 0)
                                    AL.Add("#");
                                else
                                    AL.AddRange(V);

                                RulesList[RulesList.IndexOf(RulesList[i])] = new KeyValuePair<string, List<string>>(NEW_T, new List<string>(AL));

                            }
                        }
                    }

                }
                NEW_T = "";
            }
        }

        public void Check_Grammar2()
        {
            for (int i = 0; i < Neterminale.Count(); i++)
            {
                for (int j = 0; j < RulesList.Count() - 1; j++)
                {
                    if (Neterminale[i] == RulesList[j].Key && Neterminale[i] == RulesList[j].Value[0])
                    {
                        string NEW_T =""+Neterminale[i]+"1";
                        if (Neterminale.Contains(NEW_T) == true)
                            NEW_T += "1";
                        for (int k = 0; k < RulesList.Count(); k++)
                        {
                            if (Neterminale[i] == RulesList[k].Key && k != j)
                            { 
                                //partea se adauga X'
                                RulesList[k].Value.Add(NEW_T);

                                RulesList[j].Value.RemoveRange(0, 1);
                                List<string> L = new List<string>(RulesList[j].Value);
                                L.Add(NEW_T);
                                RulesList[j] = new KeyValuePair<string, List<string>>(NEW_T, new List<string>(L));

                                Neterminale.Add(NEW_T);

                                L.Clear();
                                L.Add("#");
                                Add_Rule(NEW_T, L); 
                           }
                        }
                    }
                }
            }
        }

        public bool Check_if_Common(List<string> L, List<string> C)
        {
            int n = 0, ok = 0; int c = C.Count();
            for (int i = 0; i < C.Count(); i++)
                if (i < L.Count())
                    if (L[i] != C[i])
                       return false;
            return true;
        }


        public List<string> Check_Common(string N)
        {
            List<string> Common_List = new List<string>();
            List<string> Whatever = new List<string>();
            string Primul = "";
            
                int nr = 0;
                foreach (KeyValuePair<string, List<string>> R in RulesList)
                {
                    if (R.Key == N)
                    {
                        Whatever = new List<string>(Get_String(R.Value));
                        if (Whatever.Count() != 0)
                        {
                            if (nr == 0)
                            {
                                Common_List = new List<string>(Get_String(R.Value));
                                //MessageBox.Show(N + ": " + s);
                                Primul = Common_List[0];
                                nr++;
                            }
                            else
                            {
                                if (Primul == R.Value[0])
                                {
                                    nr++;
                                    Get_Common(R.Value, Common_List, nr);
                                }
                            }
                        }
                        else
                            break;
                    }   
                }

                if (nr != 1)
                    return Common_List;
                else
                {
                    Common_List.Clear();
                    return Common_List;
                }
        }

        public void Get_Common(List<string> Rule, List<string> Common, int nr)
        {
            int n = 0, ok = 0; int C = Common.Count();
            for (int i = 0; i < Common.Count(); i++)
            {
                if (i < Rule.Count())
                {
                    if (Rule[i] == Common[i])
                        n++;
                    else
                        ok = 1;
                }
                else
                    break;

                if (ok == 1)
                    break;
            }
         
            if (n < Common.Count())
                Common.RemoveRange(n, Common.Count() - n);
        }


        List<string> Get_String(List<string> L)
        {
            List<string> Common_List = new List<string>();

            foreach (string S in L)
                Common_List.Add(S);

            return Common_List;         
        }

        private void Begin(StreamWriter SR)
        {
            SR.WriteLine("using System;");
            SR.WriteLine("using System.Collections.Generic;");
            SR.WriteLine("using System.Text;");
            SR.WriteLine("using System.IO;");
            SR.WriteLine();

            SR.WriteLine("namespace G");
            SR.WriteLine("{");
            SR.WriteLine("\t class Program");
            SR.WriteLine("\t{");
            SR.WriteLine("\t\t int NR = 0;");
            SR.WriteLine("\t\t string Initial;");
            SR.WriteLine("\t\t string[] SIR;");
            SR.WriteLine();
            SR.WriteLine("\t\t static void Main(string[] args)");
            SR.WriteLine("\t\t {");
            SR.WriteLine("\t\t Program P = new Program();");
            SR.WriteLine("\t\tP.Initial = File.ReadAllText(\"test1.txt\");");
            SR.WriteLine("\t\t P.Initial += \" $\";");
            SR.WriteLine();
            SR.WriteLine("\t\t P.SIR = P.Initial.Split(new char[]{ ' ', '\\t', '\\r', '\\n' }, StringSplitOptions.RemoveEmptyEntries);");

            SR.WriteLine(); SR.Write("\t\t\t");
            SR.Write("P." + S); SR.WriteLine("();");
            SR.WriteLine("\t\tif (P.NR == P.SIR.Length - 1 && P.SIR[P.NR] == \"$\")");
            SR.WriteLine("\t\t\t Console.WriteLine(\"Correct\");");
            SR.WriteLine("\t\t else");
            SR.WriteLine("\t\t\t Console.WriteLine(\"Error!!!\");");
            SR.WriteLine("\t\t}");
                  
        }

        public void Generate_Code(string NEW, TextBox textBox1)
        {
       
            using (StreamWriter SR = File.AppendText(NEW))
            {
                Begin(SR);

                for (int i = 0; i < Neterminale.Count(); i++)
                {
                    SR.WriteLine();
                    string[] V = new string[0];
                    SR.WriteLine("public void " + Neterminale[i]+"()");
                    SR.WriteLine("{");
                    LookFor_In_Matrix(i, ref V);

                    int nr1 = 0, nr2;
                    for (int j = 0; j < V.Length; j++)
                    {
                        nr2 = 0;
                        if (nr1 == 0)
                        {
                            SR.WriteLine("\t if(SIR[NR] == \"" + V[j] + "\" && NR < SIR.Length)");
                            SR.WriteLine("\t {");
                            nr1++;
                        }
                        else
                        {
                            SR.WriteLine("\t}");
                            SR.WriteLine("\t else if(SIR[NR] == \"" + V[j] + "\" && NR < SIR.Length)");
                            SR.WriteLine("\t {");
                        }

                        int x;
                        if(V[j] != "$")
                            x = Convert.ToInt32(Matrix[i, Terminale.IndexOf(V[j])]);
                        else
                            x = Convert.ToInt32(Matrix[i, Terminale.Count()]);

                        for (int k = 0; k < RulesList[x - 1].Value.Count(); k++)
                            if (Neterminale.Contains(RulesList[x - 1].Value[k]) == true)
                                SR.WriteLine("\t\t" + RulesList[x - 1].Value[k] + "();\n");
                            else
                            if (Terminale.Contains(RulesList[x - 1].Value[k]) == true)
                            {
                                if (nr2 == 0 && V[j] == RulesList[x - 1].Value[k])
                                {
                                    SR.WriteLine("\t NR++;\n");
                                    nr2++;
                                }
                                else
                                    if (nr2 > 1)
                                    {
                                        SR.WriteLine("\t\t if(SIR[NR] == \"" + RulesList[x - 1].Value[k] + "\" && NR < SIR.Length)");
                                        SR.WriteLine("\t\t NR++;");
                                        SR.WriteLine("\t else");
                                        SR.WriteLine("\t{");
                                        SR.WriteLine("\t\tConsole.WriteLine(\"Eroare\");");
                                        SR.WriteLine("\t\tEnvironment.Exit(0);");
                                        SR.WriteLine("\t}");
                                     }
                                nr2++;
                            }
                        nr2 = 0;
                    }

                    SR.WriteLine("\t }");
                    SR.WriteLine("\telse");
                    SR.WriteLine("{");
                    SR.WriteLine("\t Console.WriteLine(\"Eroare\");");
                    SR.WriteLine("Environment.Exit(0);");
                    SR.WriteLine("}");
                    SR.WriteLine("}");
                                     
                }

                    
                    SR.Flush();
                       
            }

            using (StreamWriter SR = File.AppendText(NEW))
            {
                SR.WriteLine("\t}");
                SR.WriteLine("}"); 
            }

            Compile_Execute(textBox1);

        }

        private void Compile_Execute(TextBox textBox1)
        {

            if (File.Exists("NEW.cs"))
            {
                Compile("NEW.cs");

                Process compiler = new Process();
                compiler.StartInfo.FileName = System.Environment.CurrentDirectory + "\\NEW1.exe";
                compiler.StartInfo.UseShellExecute = false;
                compiler.StartInfo.RedirectStandardOutput = true;
                compiler.Start();

                //MessageBox.Show(compiler.StandardOutput.ReadToEnd());
                textBox1.Text = compiler.StandardOutput.ReadToEnd();

                compiler.WaitForExit();
            }
            else
                MessageBox.Show("Fisierul sursa lipseste - NEW.cs");
        }

        private bool Compile(string fis)
        {

            FileInfo initial = new FileInfo(fis);
            CodeDomProvider provider = null;

            if (initial.Extension.ToLower(CultureInfo.InvariantCulture) == ".cs")
                provider = CodeDomProvider.CreateProvider("CSharp");
            else
                MessageBox.Show("Fisierul sursa trebuie sa fie .cs");

            if (provider != null)
            {
                String exec = String.Format(@"{0}\{1}.exe", System.Environment.CurrentDirectory,
                     initial.Name.Replace(".cs", "1"));

                CompilerParameters param = new CompilerParameters();
                param.GenerateExecutable = true;
                param.OutputAssembly = exec;
                param.GenerateInMemory = false;
                param.TreatWarningsAsErrors = false;

                CompilerResults result = provider.CompileAssemblyFromFile(param,
                    fis);

                if (result.Errors.Count > 0)
                    MessageBox.Show("Exista erori!!!");
                else
                    MessageBox.Show("Compilare cu succes!!! Rezultatul: ");

                if (result.Errors.Count > 0)
                    return false;
                else
                    return true;
            }
            else
                return false;
        
        }

        private void LookFor_In_Matrix(int line, ref string[] V)
        {           
            for (int j = 0; j <= Terminale.Count(); j++)
                if (Matrix[line, j] != null)
                {
                    Array.Resize(ref V, V.Length + 1);
                    if(j == Terminale.Count())
                        V[V.Length - 1] = "$";    
                    else
                        V[V.Length - 1] = Terminale[j];
                }
        }
    }
}
