﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Two
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Grammar G = new Grammar();

            ReadGrammar RG = new ReadGrammar();
            RG.Reader(G);
            G.Check_Grammar1();
            G.Check_Grammar2();
            RG.Print_Grammar(listBox1, G);

            
            G.First_Follow();
            G.Print_First_Follow(listBox2);

            G.Fill_Matrix(dataGridView1);

           G.Generate_Code("NEW.cs", textBox1);
           
        }
    }
}
