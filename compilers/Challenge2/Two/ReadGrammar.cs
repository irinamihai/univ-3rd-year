﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace Two
{
    class ReadGrammar
    {
        
        List<string> l = new List<string>();
        string copie;
        List<KeyValuePair<string, List<string>>> RulesList = new List<KeyValuePair<string, List<string>>>(10);

        public void Reader(Grammar G)
        {
            using (StreamReader sr = new StreamReader("G5.txt"))
            {
                string s;
                int nr1 = 0, nr2 = 0;
                Regex r = new Regex(" ");
                
                while ((s = sr.ReadLine()) != null)
                {
                    string[] parts = r.Split(s);
                    foreach (String W in parts)
                    {
                        if (nr1 == 0)
                            G.S = W;
                        else
                            if (nr1 == 1)
                                G.Add_Neterminal(W);
                            else
                                if (nr1 == 2)
                                    G.Add_Terminal(W);
                                else
                                    if(nr1 > 3)
                                {
                                    Process_Rule(W, nr2);
                                    nr2++;
                                }                       
                    }
                    nr1++;
                    nr2 = 0;
                    if (nr1 > 4)
                    {
                        G.Add_Rule(copie, l);
                        l.Clear();
                    }
                }               
            }
        }
            public void Process_Rule(string s, int nr2)
            {
                if (nr2 == 0)
                    copie = s;
                if (nr2 >= 2)
                    l.Add(s);                           
            }

            public void Print_Grammar(ListBox listbox1, Grammar G)
            {
                G.Print_Rules(listbox1);
            }

        }
    }
